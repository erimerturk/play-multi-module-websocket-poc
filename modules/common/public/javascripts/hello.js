if (!('contains' in String.prototype)) {
    String.prototype.contains = function (str, startIndex) {
        return ''.indexOf.call(this, str, startIndex) !== -1;
    };
}
(function () {

    window.onload = function () {
        webSocketConnect();
    }
}());


function urlForWs() {
    return "ws://localhost:1234/socket"
}

function renderStandardMetric(key, value) {
    var element = document.querySelector('[data-key="' + key + '"]');
    if (element) {
        element.querySelector(".value").innerHTML = value.data + " " + value.longValue;
    } else {
        console.log("ERROR: Metric with key \"" + key + "\" given but no matching found");
        console.log("ERROR: Data is " + value);
    }
}

function updateDashboard(message) {
    var m = JSON.parse(message.data);
    renderStandardMetric(m.key, m.value);
}

var ws;

function webSocketConnect() {
    var socketUrl = urlForWs();
    ws = new WebSocket(socketUrl);

    ws.onopen = function () {
        console.log("Web socket connected");
    };

    ws.onmessage = function (event) {
        console.log("on message : ", event.data);
        updateDashboard(event)
    }

    ws.onclose = function () {
        console.log("on closed");
    }

}


if (window.console) {
    console.log("Welcome to your Play application's JavaScript!");
}