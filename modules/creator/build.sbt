name := "creator"

scalacOptions ++= Seq(
  "-feature", // Shows warnings in detail in the stdout
  "-language:reflectiveCalls" // Address the warning generated on the primary route files after introducing subprojects; see https://groups.google.com/d/msg/play-framework/nNr2NdBtWuw/JfbPLaX35XcJ
)

libraryDependencies ++= Seq(
  ws
)