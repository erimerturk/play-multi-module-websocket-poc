package controllers.creator

import play.api.Play.current
import play.api.libs.json.Json
import play.api.libs.ws.WS
import scala.concurrent.ExecutionContext.Implicits.global

import scala.util.Random

class DataCreator {

  def run () = {

    val body = Json.obj(
      "stringValue" -> Random.nextString(20),
      "longValue" ->  Random.nextLong()
    )


    println("data created : ", body.toString())

    WS.url(s"http://localhost:1234/add").post(body).map( response =>
    println(response.status))

  }

}
