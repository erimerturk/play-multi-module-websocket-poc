package controllers.creator

import akka.actor.Cancellable
import play.api.mvc._

object Application extends Controller {

  val runner = new DataCreator()

  var cancellable: Cancellable = Scheduler.start(runner.run, 2)

  def index = Action {
    Ok(views.html.common.index("this is from creator"))
  }

  def change(time:Int) = Action {

    cancellable.cancel()
    cancellable = Scheduler.start(runner.run, time)
    Ok
  }

}