package services.viewer

import models.viewer.{RandomDataHolder, Message}
import play.api.libs.concurrent.Execution.Implicits._
import play.api.libs.iteratee.Concurrent.Channel
import play.api.libs.iteratee.{Concurrent, Enumerator, Iteratee}
import play.api.libs.json._
import play.api.mvc.WebSocket
import models.viewer.Message._

object SocketChannel {
  val (out: Enumerator[String], channel: Channel[String]) = Concurrent.broadcast[String]
//  private val in = Iteratee.foreach[String] { msg => channel.push(msg)}
  private val in = Iteratee.foreach[String] { msg =>
    println(msg)
  }

  def asJsonString(m: Message) = {
    Json.stringify(Json.toJson(m))
  }


  //define push message
  def push(key: String, message: RandomDataHolder) = {
    val m = Message(key, message)
    channel.push(asJsonString(m))
  }

  //define a websocket
  def get() = {
    WebSocket.using[String] { _ =>
      (in, out)
    }
  }
}
