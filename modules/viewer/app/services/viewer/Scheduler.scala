package services.viewer

import akka.actor.ActorSystem
import play.api.libs.concurrent.Execution.Implicits._

import scala.concurrent.duration._

object Scheduler {
  val system = ActorSystem("scheduler")

  def start(f: () => Any, frequency: Int = Settings.pollFrequency) = {
    system.scheduler.schedule(0 seconds, frequency seconds) {

      f()
    }

  }
}
