package models.viewer

import akka.actor.{Actor, ActorRef}
import play.api.libs.json.Json
import services.viewer.SocketChannel

import scala.collection.mutable

class SimpleDataSender extends Actor {

  val que = new mutable.Queue[RandomDataHolder]
  def run() = {

    if (que.nonEmpty) {

      val value: RandomDataHolder = que.dequeue()
      SocketChannel.push("random-data", value)

    }else{

      println("empty queue send me data!")

    }

  }

  def receive = {
    case message:RandomDataHolder => {

      println(s"yeni mesaj : ${message.data} -  ${message.longValue}")
      que.enqueue(message)
    }
    case SendData => run()
  }


}


class SimpleDataSenderRunner(actorRef: ActorRef) {

  def run() = {

    actorRef ! SendData

  }

}


case class RandomDataHolder(data:String, longValue: Long)
object RandomDataHolder {
  implicit val dataFmt = Json.format[RandomDataHolder]
}

case object SendData