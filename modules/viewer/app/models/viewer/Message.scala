package models.viewer

import play.api.libs.json.Json

case class Message(key: String, value: RandomDataHolder)

object Message {
  implicit val dataFmt = Json.format[Message]
}
