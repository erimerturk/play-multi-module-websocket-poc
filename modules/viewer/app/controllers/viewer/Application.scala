package controllers.viewer

import akka.actor.{Cancellable, Props}
import models.viewer.{RandomDataHolder, SimpleDataSenderRunner, SimpleDataSender}
import play.api.libs.json.{JsPath, JsError}
import play.api.libs.functional.syntax._
import play.api.mvc._
import services.viewer.{SocketChannel, Scheduler}

object Application extends Controller {

  val b = Scheduler.system.actorOf(Props[SimpleDataSender], name = "simple-widget")
  val runner = new SimpleDataSenderRunner(b)

  var cancellable: Cancellable = Scheduler.start(runner.run, 2)


  def index = Action {
    Ok(views.html.common.index("This is from viewer"))
  }

  def change(time:Int) = Action {

    cancellable.cancel()
    cancellable = Scheduler.start(runner.run, time)
    Ok
  }

  def add = Action {  request =>

    val stringValue = request.body.asJson.map(json => (json \ "stringValue").as[String]).get
    val longValue = request.body.asJson.map(json => (json \ "longValue").as[Long]).get

    b ! RandomDataHolder(stringValue, longValue)
    Ok

//    request.body.asJson.map(json => RandomDataHolder(json.\("stringValue"), json.\("longValue").)
//
//    println(rand)
//
//    rand match {
//      case input:Option[RandomDataHolder] => {
//
//        b ! input.get
//        Ok
//      }
//      case _ =>
//        BadRequest("Expecting Json data")
//    }

  }


  def socket = SocketChannel.get()

}