name := "websocket-poc"

version := "1.0-SNAPSHOT"

lazy val root = (project in file("."))
  .enablePlugins(PlayScala)
  .aggregate(common, viewer, creator)
  .dependsOn(common, viewer, creator)

lazy val common = project.in(file("modules/common")).enablePlugins(PlayScala)
lazy val viewer = project.in(file("modules/viewer")).enablePlugins(PlayScala)
  .dependsOn(common)
lazy val creator = project.in(file("modules/creator")).enablePlugins(PlayScala)
  .dependsOn(common)


scalaVersion := "2.11.1"

libraryDependencies ++= Seq(
  ws
)
